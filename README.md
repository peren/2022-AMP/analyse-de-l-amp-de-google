# AMP: Du dopage au sevrage

Ce dépôt contient tout le code pour reproduire les expériences menées dans le cadre de l’étude conduite par le [PEReN](https://www.peren.gouv.fr/) sur le _framework_ Accelerated Mobile Pages (AMP) développé par Google. L’étude complète est accessible [ici](https://www.peren.gouv.fr/actualites/2022-10-17_eclairage_sur_amp/).

## Utilisation
Commencer par installer toutes les dépendances : 
```
python -m venv .venv
source .venv/bin/activate
pip install --upgrade pip wheel
pip install -r requirements.txt
npm ci
```

### Benchmark de référencement
```
python amp/get_results_for_query.py <output_file> <query_file> --all
```
Dans ce cas précis, l’activation de l‘option `--all` induira la sauvegarde de tous les résultats *desktop* dans le fichier `output.json` et de tous les résultats mobile dans `output_mobile.json`.
Un fichier exemple `example_queries.txt` se trouve dans le répertoire `data/`.

### Benchmark de performance 
Un premier script utilise l’outil de Google _Ligthouse_ afin de mesurer la performance d’une page. Pour tester la performance de différentes pages, le script peut être utilisé comme suit :  
```
./amp/get_lighthouse_stats.sh <url1> <url2>  ... <urln>
```
Ce script retourne un nom de dossier dans lequel les fichiers JSON de Lighthouse ont été stockés.
Pour prétraiter ces fichiers, il est ensuite nécessaire de faire appel au fichier `amp/utils/parse_lighthouse_audits.py`.
 ```
python amp/utils/parse_lighthouse_audits.py <folder_name> <output_file>
```
cela sauvegarde un *dataframe* dans un fichier CSV qui contient les principales métriques de performance (FCP, LCP, TTI, SI)

### Notebooks
Le dépôt contient deux _notebooks_ (dans `notebooks/`) et les données nécessaires à leur bon fonctionnement (situées dans `data/`).

Le notebook `ranking_comparison.ipynb` montre l’analyse de données qui peut être faite pour comparer le rang d’apparition de résultats pour différentes requêtes sur mobile et sur desktop, en mettant en perspective ces rangs d‘apparition avec les performances de pages et l’utilisation ou non par l’éditeur d’AMP.

Le notebook `performance_comparison.ipynb` analyse à partir de données collectées les différences de performance entre les différentes variantes, en se fondant notamment sur les _Core Web Vitals_.

### Contribution

Pour contribuer au dépôt : 

```
pip install -r requirements-dev.txt
pre-commit install
```
