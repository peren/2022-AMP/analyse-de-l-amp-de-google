bs4==0.0.1
pandas==1.4.3
matplotlib==3.5.2
requests==2.28.1
altair==4.2.0
