from amp.utils.urls import generate_amp_from_cache, generate_cache_from_amp

BASE_AMP_TEST_URL = "https://www.peren.gouv.fr/amp/test.amp"
CACHE_AMP_TEST_URL = "https://www-peren-gouv-fr.cdn.ampproject.org/c/s/www.peren.gouv.fr/amp/test.amp"


def test_base_amp_to_amp_cache():
    cache_url = generate_cache_from_amp(BASE_AMP_TEST_URL)
    assert cache_url == CACHE_AMP_TEST_URL

def test_amp_cache_to_base_amp():
    base_url = generate_amp_from_cache(CACHE_AMP_TEST_URL)
    assert base_url == BASE_AMP_TEST_URL

