#
# The MIT License (MIT)
#
# Copyright (c) 2022 PEReN
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
from base64 import b64encode
from datetime import datetime
import json

SAMSUNG_HEADER = (
    "Mozilla/5.0 (Linux; Android 5.0; SM-G900P Build/LRX21T) AppleWebKit/537.36"
    + "(KHTML, like Gecko) Chrome/85.0.4183.121 Mobile Safari/537.36"
)
DESKTOP_HEADER = "Mozilla/5.0 (X11; Linux x86_64; rv:101.0) Gecko/20100101 Firefox/101.0"


class ResultsPage:
    """
    Generic class used to structure AMP pages collection
    from Google News and Google Search results pages.
    """

    def __init__(self, query: str, mobile: bool):
        self.query = query
        self.mobile = mobile
        self.results = None
        self._set_consent_cookie()
        self._set_headers_for_device()

    def get(self):
        """
        Returns the plain text content of the
        results page using a GET query
        """
        raise NotImplementedError

    def parse(self, raw_content: str):
        """
        Parses the raw content from the page
        to extract structured information regarding
        AMP pages rankings
        """
        raise NotImplementedError

    def _set_headers_for_device(self):
        """
        Sets headers based on self.mobile
        """
        self.headers = {
            "cookie": self.consent_cookie + ";sec-ch-ua-model=SM-G900P",
            "user-agent": SAMSUNG_HEADER if self.mobile else DESKTOP_HEADER,
        }

    def _set_consent_cookie(self):
        bit_field = b"\x08\x02\x12\x1c\x08\x02\x12\x12gws_"
        full_string = bit_field + bytes(datetime.today().strftime("%Y%m"), encoding="utf-8")
        self.consent_cookie = "SOCS=" + b64encode(full_string).decode("utf-8")

    def save(self, output_file: str):
        """
        Saves results in a JSON file
        """
        if self.results is None:
            raise Exception("Results have not been computed yet!")

        if not output_file.endswith("json"):
            raise Exception("Output filename must end with JSON")

        with open(output_file, "w") as f:
            json.dump(self.results, f, indent=2)

    def run_pipeline(self, save: bool = False, output_file: str = None):
        """
        Performs a request, parses the raw content
        and saved the resulting structured content
        to a JSON file
        """
        content = self.get()
        self.parse(content)
        if save and output_file is not None:
            self.save(output_file)
