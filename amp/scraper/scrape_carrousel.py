#
# The MIT License (MIT)
#
# Copyright (c) 2022 PEReN
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
import requests
from bs4 import BeautifulSoup
from .results import ResultsPage

NEWS_ELEMENT = "a"
CAROUSEL_MOBILE_ELEMENT = "g-scrolling-carousel"
CAROUSEL_MOBILE_CLASS = "kQ9KOd"
CAROUSEL_DESKTOP_ELEMENT = "g-section-with-header"
RESULT_DIV_CLASS = "WlydOe"
AMP_PREFIX = "amp_r"
DEFAULT_NON_AMP_WIDTH = 16


class GoogleSearchCarrouselResults(ResultsPage):
    def __init__(self, query: str, mobile: bool):
        super().__init__(query, mobile)

    def get(self) -> str:
        search_url = (
            f"https://www.google.com/search?q={self.query}&oq={self.query}"
            + "&sclient=mobile-gws-wiz-serp" * self.mobile
        )

        response = requests.get(search_url, headers=self.headers)

        return response.content

    def parse(self, content: str) -> dict:
        soup = BeautifulSoup(content)

        if self.mobile:
            data = soup.find_all(CAROUSEL_MOBILE_ELEMENT, class_=CAROUSEL_MOBILE_CLASS)[0].find_all(NEWS_ELEMENT)
        else:
            data = soup.find_all(CAROUSEL_DESKTOP_ELEMENT)[0].find_all(NEWS_ELEMENT)

        current_result = []
        first_rank = None

        for rank, page in enumerate(data):
            if not page.has_attr("class") or RESULT_DIV_CLASS not in page["class"]:
                continue

            if first_rank is None:
                first_rank = rank

            image_width = -1
            media_name = page["href"].split("/")[2]
            flag_amp = AMP_PREFIX in page["class"]

            if flag_amp:
                if len(page.find_all("img", width=DEFAULT_NON_AMP_WIDTH)) == 0:
                    logo = page.find_all("img")[-1]
                    image_width = logo["width"]

            current_result.append(
                {
                    "amp": flag_amp,
                    "url": page["href"],
                    "domain": media_name,
                    "favicon_width": int(image_width),
                }
            )

        self.results = current_result
