#
# The MIT License (MIT)
#
# Copyright (c) 2022 PEReN
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
import requests
import logging
from urllib.parse import urlparse, urljoin
from bs4 import BeautifulSoup


def generate_canonical_from_amp(amp_url: str) -> str:
    content = requests.get(amp_url).content
    soup = BeautifulSoup(content)

    canonical_url = soup.find("link", {"rel": "canonical"})
    if canonical_url is not None:
        return canonical_url["href"]
    else:
        logging.warning(f"Failed to get canonical url for {amp_url}")
        return ""


def generate_amp_from_cache(cache_url: str) -> str:
    prefix = "cdn.ampproject.org/c/s/"
    if prefix not in cache_url:
        raise Exception("URL does not mactch Google AMP cache url pattern")
    return "https://" + cache_url.split(prefix)[1]


def generate_cache_from_amp(amp_url: str) -> str:
    parsed_url = urlparse(amp_url)
    domain = parsed_url.hostname.replace("-", "--").replace(".", "-")
    end_url = amp_url.removeprefix(f"{parsed_url.scheme}://")
    base_url = urljoin(f"https://{domain}.cdn.ampproject.org/c/s/", end_url)
    return base_url


def generate_amp_from_viewer(viewer_url: str) -> str:
    viewer_prefix = "google.com/amp/s/"
    if viewer_prefix not in viewer_url:
        raise Exception(f"URL does not match viewer url pattern ({viewer_prefix})")

    return urljoin("https://", viewer_url.split(viewer_prefix)[1])


def get_domain_from_url(url: str) -> str:
    return urlparse(url).hostname
