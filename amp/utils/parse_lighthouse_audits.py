#
# The MIT License (MIT)
#
# Copyright (c) 2022 PEReN
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
import os
import json
import logging
import pandas as pd
import argparse
from pathlib import Path


def parse_lighthouse_json(data: dict) -> dict:
    """
    Retrieves the main KPIs from lighthouse JSON
    - First Contentful Paint (FCP)
    - Largest Contentful Paint (LCP)
    - Time To Interactive (TTI)
    - Speed Index (SI)
    """
    fcp = data["audits"]["first-contentful-paint"]["numericValue"]
    lcp = data["audits"]["largest-contentful-paint"]["numericValue"]
    tti = data["audits"]["interactive"]["numericValue"]
    speed_index = data["audits"]["speed-index"]["numericValue"]
    layout_shifts = data["audits"]["cumulative-layout-shift"]["numericValue"]
    fid = data["audits"]["max-potential-fid"]["numericValue"]
    perf = float(data["categories"]["performance"]["score"])

    return {
        "fcp": fcp,
        "lcp": lcp,
        "tti": tti,
        "si": speed_index,
        "cls": layout_shifts,
        "fid": fid,
        "score": perf,
    }


def process_all_files(folder_name: str):
    audit_data = {}

    for current_file in os.listdir(folder_name):
        try:
            with open(Path(folder_name).joinpath(current_file), "r") as f:
                data = json.load(f)
            run = current_file.split("_")[-1].split(".")[0]
            url = data["requestedUrl"]
            cache = "ampproject" in url
            amp = "amp" in url
            audit_data[(url, run)] = parse_lighthouse_json(data)
            audit_data[(url, run)].update({"cache": cache, "amp": amp})
        except Exception as e:
            logging.warning(f"{current_file} is invalid. ({e})")
            continue

    return (
        pd.DataFrame(audit_data.values(), index=audit_data.keys())
        .reset_index()
        .rename(columns={"level_0": "url", "level_1": "run"})
        .sort_values(["url", "run"])
    )


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("folder")
    parser.add_argument("output_file")
    args = parser.parse_args()

    folder_name = Path(args.folder)

    if not args.output_file.endswith("csv"):
        raise Exception("Output file must end with csv extension!")

    audits = process_all_files(args.folder)
    audits.to_csv(args.output_file)
