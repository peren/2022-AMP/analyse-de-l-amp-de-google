#
# The MIT License (MIT)
#
# Copyright (c) 2022 PEReN
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
import argparse
import json
from typing import List

from scraper.scrape_carrousel import GoogleSearchCarrouselResults
from scraper.scrape_news import GoogleNewsResultsPage


def load_queries(file_path: str):
    with open(file_path, "r") as f:
        queries = list(map(str.strip, f.readlines()))
    return queries


def save_results(results: dict, output_file: str):
    with open(output_file, "w") as f:
        json.dump(results, f, indent=2)


def get_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument("output_file", help="Output file where results are stored")
    parser.add_argument("query_file", help="Query file")
    parser.add_argument("--mobile", action="store_true", help="Either search on mobile or desktop")
    parser.add_argument(
        "--news",
        action="store_true",
        help="Either Google Search or Google News (default is Google Search)",
    )
    parser.add_argument(
        "--all",
        action="store_true",
        help='Tests both mobile and desktop versions (ignores "--mobile" argument)',
    )

    return parser


def process_queries(queries: List[str], parsed_args: dict) -> dict:
    results = []

    for query in queries:
        if parsed_args.news:
            page = GoogleNewsResultsPage(query, parsed_args.mobile)
        else:
            page = GoogleSearchCarrouselResults(query, parsed_args.mobile)

        page.run_pipeline(save=False)
        results.append({"query": query, "mobile": parsed_args.mobile, "data": page.results})

        if parsed_args.all:
            if parsed_args.news:
                page = GoogleNewsResultsPage(query, not parsed_args.mobile)
            else:
                page = GoogleSearchCarrouselResults(query, not parsed_args.mobile)

            page.run_pipeline(save=False)
            results.append({"query": query, "mobile": not parsed_args.mobile, "data": page.results})

    results = {
        "experiment": {
            "news": parsed_args.news,
            "queries": queries,
        },
        "results": results,
    }

    return results


if __name__ == "__main__":
    parser = get_parser()
    parsed_args = parser.parse_args()
    queries = load_queries(parsed_args.query_file)
    results = process_queries(queries, parsed_args)
    save_results(results, parsed_args.output_file)
